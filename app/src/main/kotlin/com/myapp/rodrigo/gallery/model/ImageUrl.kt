package com.myapp.rodrigo.gallery.model

class ImageUrl {
    var owner: String? = ""
    var url: String? = ""
}