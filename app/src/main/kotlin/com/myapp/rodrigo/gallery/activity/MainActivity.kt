package com.myapp.rodrigo.gallery.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import com.mcxiaoke.koi.ext.quickAdapterOf
import com.myapp.rodrigo.gallery.R
import com.myapp.rodrigo.gallery.model.ImageUrl
import com.myapp.rodrigo.gallery.modelview.MainViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), Observer<List<ImageUrl>> {

    private val viewModel: MainViewModel by lazy { ViewModelProviders.of(this).get(MainViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel.imageListData.observe(this, this)
    }

    override fun onChanged(imageUrlList: List<ImageUrl>?) {
        imageUrlList?.let { list ->
            val urls = list.map { it.url!! }
            imageList.adapter = quickAdapterOf(R.layout.image_view, urls) { view, url ->
                Picasso.with(view.context).load(url).into(view.view.findViewById(R.id.imageView) as ImageView)
            }
        }
    }
}
