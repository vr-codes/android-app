package com.myapp.rodrigo.gallery.modelview

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth

class LoginModelView : ViewModel() {

    val loginSucces = MutableLiveData<String>()

    fun login(username: String, password: String) {
        FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(username, password)
                .addOnSuccessListener { authResult -> loginSucces.value = authResult.user.uid }
                .addOnFailureListener { loginSucces.value = null }

    }

    fun getCurrentUserId() = FirebaseAuth.getInstance().currentUser?.uid

}