package com.myapp.rodrigo.gallery.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.mcxiaoke.koi.ext.onClick
import com.mcxiaoke.koi.ext.startActivity
import com.mcxiaoke.koi.ext.toast
import com.myapp.rodrigo.gallery.R
import com.myapp.rodrigo.gallery.modelview.LoginModelView
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), Observer<String?> {

    private val viewModel: LoginModelView by lazy { ViewModelProviders.of(this).get(LoginModelView::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (viewModel.getCurrentUserId() != null) {
            startActivity<MainActivity>()
        } else {
            loginButton.onClick { viewModel.login(usernameInput.text.toString(), passwordInput.text.toString()) }
            viewModel.loginSucces.observe(this, this)
        }
    }

    override fun onChanged(userId: String?) {
        if (userId != null)
            startActivity<MainActivity>()
        else
            toast(R.string.login_failed)
    }
}
