package com.myapp.rodrigo.gallery.modelview

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.myapp.rodrigo.gallery.model.ImageUrl

class MainViewModel : ViewModel() {

    val imageListData = MutableLiveData<List<ImageUrl>>()

    private val database = FirebaseFirestore.getInstance().collection("links")

    init {
        FirebaseAuth.getInstance().currentUser?.let {
            database.whereEqualTo(ImageUrl::owner.name, it.uid).addSnapshotListener { data, _ ->
                data?.documents?.let { changeList(data.documents) }
            }
        }
    }

    private fun changeList(documents: List<DocumentSnapshot>) {
        imageListData.value = documents.map { it.toObject(ImageUrl::class.java)!! }
    }

}